#include <stdio.h>
#include <stdlib.h>
#include "cam.h"

int main(){
  struct Camisa cam;
  int capacidade, op;
	float valor;

	printf( "\nCapacidade da pilha? " );
	scanf( "%d", &capacidade );

	criarcam (&cam, capacidade);

	while( 1 ){ /* loop infinito */

		printf("\n1- empilhar (push)\n");
		printf("2- desempilhar (POP)\n");
		printf("3- Mostrar o topo \n");
		printf("4- sair\n");
		printf("\nopcao? ");
		scanf("%d", &op);

		switch (op){

			case 1: //push

				if( estacheia( &cam ) == 1 )

					printf("\nPILHA CHEIA! \n");

				else {

					printf("\nVALOR? ");
					scanf("%f", &valor);
					empilhar (&cam, valor);

				};
				break;

			case 2: //pop
				if ( estavazia(&cam) == 1 )

					printf( "\nPILHA VAZIA! \n" );

				else{

					valor = desempilhar (&cam);
					printf ( "\n%.1f DESEMPILHADO!\n", valor );

				};
				break;

			case 3: // mostrar o topo
				if ( estavazia (&cam) == 1 )

					printf( "\nPILHA VAZIA!\n" );

				else {

					valor = retornatopo (&cam);
					printf ( "\nTOPO: %.1f\n", valor );

				};
				break;

			case 4:
				exit(0);

			default: printf( "\nOPCAO INVALIDA! \n" );
		};
	};

};
